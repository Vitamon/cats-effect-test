package forex.programs.rates

import forex.services.rates.errors.{ Error => RatesServiceError }

object errors {

  sealed trait ApiError extends Exception
  object ApiError {
    final case class BadRequest(msg: String) extends ApiError
    final case class InternalServerError(msg: String) extends ApiError
    final case object NotFound extends ApiError
  }

  def toProgramError(error: RatesServiceError): ApiError = error match {
    case RatesServiceError.OneFrameLookupFailed(msg) => ApiError.InternalServerError(msg)
    case RatesServiceError.OneFrameLookupNotFound => ApiError.NotFound
  }
}
