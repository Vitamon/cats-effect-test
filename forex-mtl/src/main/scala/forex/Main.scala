package forex

import cats.effect._
import cats.effect.concurrent.Ref
import forex.config._
import forex.domain.Rate
import forex.services.rates.errors
import forex.services.rates.errors.Error.OneFrameLookupNotFound
import forex.services.rates.interpreters.{ OneFrameLiveService, OneFrameLiveWorker, RatesClient }
import fs2.Stream
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.server.blaze.BlazeServerBuilder

import scala.concurrent.ExecutionContext

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    new Application().stream(executionContext).compile.drain.as(ExitCode.Success)

}

class Application(implicit val c: ConcurrentEffect[IO], t: Timer[IO], cs: ContextShift[IO]) {

  def stream(ec: ExecutionContext): Stream[IO, Unit] =
    for {
      config <- Config.stream("app")
      httpClient <- Stream.resource(BlazeClientBuilder[IO](ec).resource)
      client = new RatesClient[IO](httpClient, config.oneFrameService)
      state <- Stream.eval(Ref.of[IO, Either[errors.Error, Seq[Rate]]](Left(OneFrameLookupNotFound)))
      worker = new OneFrameLiveWorker(client, state, config.oneFrameService)
      reader = new OneFrameLiveService(state)
      _ <- Stream.eval(worker.run().start)
      module = new Module(config, reader)
      _ <- BlazeServerBuilder[IO](ec)
            .bindHttp(config.http.port, config.http.host)
            .withHttpApp(module.httpApp)
            .serve
    } yield ()

}
