package forex.services.rates.interpreters

import cats.effect.Sync
import cats.effect.concurrent.Ref
import cats.syntax.all._
import forex.domain.Rate
import forex.services.rates.Algebra
import forex.services.rates.errors._
import forex.services.rates.errors.Error.OneFrameLookupNotFound

class OneFrameLiveService[F[_]: Sync](state: Ref[F, Either[Error, Seq[Rate]]]) extends Algebra[F] {

  override def get(pair: Rate.Pair): F[Error Either Rate] =
    for {
      ratesE <- state.get
    } yield
      ratesE
        .flatMap { rates =>
          rates.find(_.pair == pair).toRight(OneFrameLookupNotFound)
        }

}
