package forex.services.rates.interpreters

import cats.effect.Sync
import cats.implicits._
import forex.config.OneFrameConfig
import forex.domain.{ Price, Rate, Timestamp }
import forex.services.rates.errors.Error
import forex.services.rates.errors.Error.OneFrameLookupFailed
import forex.services.rates.interpreters.RatesResponses.{ RateResponse, _ }
import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder
import org.http4s.{ EntityDecoder, Header, Uri }
import org.http4s.Method.GET
import org.http4s.circe.jsonOf
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.util.CaseInsensitiveString

import java.time.{ LocalDateTime, OffsetDateTime, ZoneOffset }
import java.time.format.DateTimeFormatter
import scala.collection.immutable.Seq

object RatesResponses {

  case class RateResponse(from: String, to: String, price: BigDecimal, time_stamp: LocalDateTime) {
    def pairKey(): String = s"$from$to".toUpperCase
  }

  case class ErrorResponse(error: String)

  // "2021-05-21T21:32:40.123Z"
  implicit lazy val dtFormat: DateTimeFormatter             = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
  implicit val OffsetDateTimeFormat: Decoder[LocalDateTime] = Decoder[String].map(s => LocalDateTime.parse(s, dtFormat))

  implicit lazy val rateDecoder: Decoder[RateResponse]   = deriveDecoder
  implicit lazy val errorDecoder: Decoder[ErrorResponse] = deriveDecoder

  implicit lazy val respDecoder: Decoder[Either[ErrorResponse, Seq[RateResponse]]] =
    errorDecoder.either(implicitly[Decoder[Seq[RateResponse]]])

  val defaultZoneOffset: ZoneOffset = ZoneOffset.UTC
}

class RatesClient[F[_]: Sync](httpClient: Client[F], config: OneFrameConfig) extends Http4sClientDsl[F] {

  implicit lazy val rateEntityDecoder: EntityDecoder[F, Either[ErrorResponse, Seq[RateResponse]]] = jsonOf

  def fetch(pairs: Seq[Rate.Pair]): F[Error Either Seq[Rate]] = {

    val request = GET(
      Uri
        .unsafeFromString(s"http://${config.http.host}:${config.http.port}")
        .withPath("rates")
        .withMultiValueQueryParams(Map("pair" -> pairs.map(_.show))),
      Header.Raw(CaseInsensitiveString("token"), config.token)
    )

    val showToPair = pairs.map(p => p.show -> p).toMap

    httpClient.expect[Either[ErrorResponse, Seq[RateResponse]]](request).map {
      case Left(ErrorResponse(error)) =>
        Left(OneFrameLookupFailed(error))

      case Right(responses) =>
        Right(responses.flatMap { p =>
          showToPair.get(p.pairKey()).map { pair =>
            Rate(pair, Price(p.price), Timestamp(OffsetDateTime.of(p.time_stamp, defaultZoneOffset)))
          }
        })
    }
  }
}
