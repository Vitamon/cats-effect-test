package forex.services.rates.interpreters

import cats.effect.{ IO, Timer }
import cats.effect.concurrent.Ref
import forex.config.OneFrameConfig
import forex.domain.Rate
import forex.domain.Rate.AllPairs
import forex.services.rates.errors

class OneFrameLiveWorker(client: RatesClient[IO],
                         state: Ref[IO, Either[errors.Error, Seq[Rate]]],
                         config: OneFrameConfig)(
    implicit t: Timer[IO]
) {

  def run(): IO[Unit] =
    for {
      _ <- IO(println(s"${Thread.currentThread()} loading new data"))
      newPairs <- client.fetch(AllPairs)
      _ <- state.set(newPairs)
      _ <- IO.sleep(config.fetchInterval)
      _ <- run()
    } yield ()

}
