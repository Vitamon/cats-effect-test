package forex.config

import scala.concurrent.duration.FiniteDuration

case class ApplicationConfig(
    http: HttpConfig,
    oneFrameService: OneFrameConfig
)

case class OneFrameConfig(
    http: HttpConfig,
    token: String,
    fetchInterval: FiniteDuration
)

case class HttpConfig(
    host: String,
    port: Int,
    timeout: FiniteDuration
)
