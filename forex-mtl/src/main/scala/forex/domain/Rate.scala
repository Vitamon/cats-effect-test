package forex.domain

import cats.Show
import cats.implicits.toShow
import forex.domain.Currency.supportedCurrencies

case class Rate(
    pair: Rate.Pair,
    price: Price,
    timestamp: Timestamp
)

object Rate {
  final case class Pair(
      from: Currency,
      to: Currency
  )

  implicit val show: Show[Pair] = Show.show { pair =>
    s"${pair.from.show}${pair.to.show}"
  }

  val AllPairs: Seq[Pair] = (for {
    c1 <- supportedCurrencies
    c2 <- supportedCurrencies if c1 != c2
  } yield Pair(c1, c2)).toSeq

}
