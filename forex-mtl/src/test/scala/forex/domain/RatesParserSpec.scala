package forex.domain

import forex.services.rates.interpreters.RatesResponses
import forex.services.rates.interpreters.RatesResponses._
import io.circe.parser._
import org.scalatest.flatspec._
import org.scalatest.matchers._

import java.time.LocalDateTime

class RatesParserSpec extends AnyFlatSpec with should.Matchers {

  "Rates parser" should "parse success json response" in {
    val jsonStr =
      """[{"from":"USD","to":"EUR","bid":0.6118225421857174,"ask":0.8243869101616611,"price":0.71810472617368925,"time_stamp":"2021-05-21T21:32:40.123Z"}]"""

    val decoded = decode[Either[ErrorResponse, Seq[RateResponse]]](jsonStr)

    decoded shouldBe Right(
      Right(
        List(
          RateResponse(
            "USD",
            "EUR",
            BigDecimal("0.71810472617368925"),
            LocalDateTime.parse("2021-05-21T21:32:40.123Z", RatesResponses.dtFormat)
          )
        )
      )
    )
  }

  it should "parse error response" in {
    val jsonStr =
      """{"error":"Invalid Currency Pair"}"""

    val decoded = decode[Either[ErrorResponse, Seq[RateResponse]]](jsonStr)

    decoded shouldBe Right(
      Left(
        ErrorResponse("Invalid Currency Pair")
      )
    )
  }

}
